package com.trilda.eggscooker;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static org.junit.Assert.assertEquals;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import kotlin.jvm.JvmField;
import tools.fastlane.screengrab.Screengrab;
import tools.fastlane.screengrab.locale.LocaleTestRule;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
class EggsCookerInstrumentedTest {

    @ClassRule
    public static final LocaleTestRule localeTestRule = new LocaleTestRule();

    @Rule
    public ActivityScenarioRule<MainActivity> rule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void  testTakeScreenshot() {
        Screengrab.screenshot("before_button_click");

        // Your custom onView...
        onView(withId(R.id.main_activity)).perform(click());

        Screengrab.screenshot("after_button_click");
    }
    
    @Test
    public void useAppContext() {
        // Context of the app under test.
        var packageName = InstrumentationRegistry.getInstrumentation().getTargetContext().getPackageName();
        assertEquals("com.trilda.eggscooker", packageName);
    }
}
