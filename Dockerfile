# This Dockerfile creates a static build image for CI

FROM openjdk:11-jdk

RUN echo ${PWD}

# Just matched `app/build.gradle`
# ANDROID_COMPILE_SDK is the version of Android you're compiling with.
# It should match compileSdkVersion.
ENV ANDROID_COMPILE_SDK: "31"

# Just matched `app/build.gradle`
# ANDROID_BUILD_TOOLS is the version of the Android build tools you are using.
# It should match buildToolsVersion.
ENV ANDROID_BUILD_TOOLS "31.0.0"

# It's what version of the command line tools we're going to download from the official site.
# Official Site-> https://developer.android.com/studio/index.html
# There, look down below at the cli tools only, sdk tools package is of format:
#        commandlinetools-os_type-ANDROID_SDK_TOOLS_latest.zip
# when the script was last modified for latest compileSdkVersion, it was which is written down below
ENV ANDROID_SDK_TOOLS "7583922"

ENV ANDROID_HOME /android-home


# install OS packages
RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget apt-utils tar unzip lib32stdc++6 lib32z1 build-essential ruby ruby-dev
# We use this for xxd hex->binary
RUN apt-get --quiet install --yes vim-common
# install Android SDK
RUN install -d $ANDROID_HOME
RUN cd ${ANDROID_HOME}
#RUN wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/repository/sdk-tools-linux/android-sdk_r${ANDROID_SDK_TOOLS}-linux.tgz
RUN wget --quiet --output-document=cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip
#RUN tar --extract --gzip --file=android-sdk.tgz
RUN unzip -d $ANDROID_HOME cmdline-tools.zip

RUN echo ${PWD}

RUN ls -alf
RUN ls -alf ${ANDROID_HOME}/cmdline-tools/bin/
RUN ls -alf ${ANDROID_HOME}/
ENV PATH=$PATH:${ANDROID_HOME}/cmdline-tools/bin/
ENV PATH="${PATH}:${ANDROID_HOME}/platform-tools/"

RUN echo ${PATH}
RUN echo ${PWD}

RUN echo "platforms;android-${ANDROID_COMPILE_SDK}"


RUN yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses || true


RUN sdkmanager --sdk_root=${ANDROID_HOME} --update
RUN sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-31"
RUN sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools"
RUN sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;31.0.0"
#RUN sdkmanager --sdk_root=${ANDROID_HOME} "extras-android-m2repository"
#RUN sdkmanager --sdk_root=${ANDROID_HOME} "extras-google-google_play_services"
#RUN sdkmanager --sdk_root=${ANDROID_HOME} "extras-google-m2repository"

# install FastLane
COPY Gemfile.lock .
COPY Gemfile .
RUN gem install bundler -v 1.16.6
RUN bundle install
